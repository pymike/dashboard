/**
 * Canvas dimensions.
*/
var pieCanvas = document.getElementById("pie");
pieCanvas.height = 300;

var cropsCanvas = document.getElementById("crops");
cropsCanvas.height = 300;

/**
 * General Configurations for Pie Chart
*/
var pieChartOptions = {
  title: {
    display: false
  },
  legend: {
    display: true,
    position: 'bottom'
  },
  layout: {
    padding: {
      left: 0,
      top: 0,
    }
  }
};

/**
 * General Configurations for Agent Performance Line Chart
*/
var agentPerformanceLineChart = {
  elements: {
    line: {
      tension: 0.000001
    }
  },
  scales: {
    xAxes: [
      {
        ticks: {
          autoSkip: false,
          maxRotation: 0,
          callback: function(value, index, values) {
            return value.toFixed(1);
          }
        },
        gridLines: {
          display: false
        }
      }
    ],
    yAxes: [
      {
        gridLines: {
          display: false
        },
        ticks: {
          max: 15.0,
          min: 0,
          stepSize: 2.5,
          callback: function(value, index, values) {
            return value.toFixed(2);
          }
        }
      }
    ]
  }
};

/**
 * Farmer Performance Layway Pie Chart
*/
var performanceData = {
  labels: ["Below Target", "Reached Goal", "On Target"],
  datasets: [
    {
      label: "Farmers Performance Layway",
      backgroundColor: ["#e93f33", "#529ee6", "#68c1f4"],
      data: [30, 50, 53]
    }
  ]
};

new Chart(document.getElementById("pie"), {
  type: "pie",
  data: performanceData,
  options: pieChartOptions
});

/**
 * Crop Enrollment Pie Chart
*/
var cropsData = {
  labels: ["Potatoes", "Beans", "Maize"],
  datasets: [
    {
      label: "Crop Enrollment",
      backgroundColor: ["#887bc9", "#529ee6", "#68c1f4"],
      data: [80, 40, 60]
    }
  ]
};

new Chart(document.getElementById("crops"), {
  type: "pie",
  data: cropsData,
  options: pieChartOptions
});

/**
 * Agent Performance Line Bar Chart
*/
var xAxesLabels = Array.from({ length: 13 }, function(value, index) {
  return index / 2;
});

var values = ["Below Target", "Reached Goal", "On Target"];

var firstData = Array.from({ length: 13 }, function() {
  return Math.floor(Math.random() * (14 - 0.5 + 1)) + 0.5;
});

var secondData = Array.from({ length: 13 }, function() {
  return Math.floor(Math.random() * (14 - 0.5 + 1)) + 0.5;
});

new Chart(document.getElementById("line"), {
  type: "line",
  data: {
    labels: xAxesLabels,
    datasets: [
      {
        backgroundColor: "rgba(147, 212, 247, .35)",
        borderColor: "rgb(147, 212, 247)",
        borderWidth: 1,
        data: firstData,
        label: "Dataset",
        fill: "start"
      },
      {
        backgroundColor: "rgba(189, 204, 236, .35)",
        borderColor: "rgb(189, 204, 236)",
        borderWidth: 1,
        data: secondData,
        fill: "start"
      }
    ]
  },
  options: Chart.helpers.merge(agentPerformanceLineChart, {
    title: {
      display: false
    },
    legend: {
      display: false
    }
  })
});

/**
 * Crop Enrollment Pie Chart
*/
var cropsData = {
  labels: ["Potatoes", "Beans", "Maize"],
  datasets: [
    {
      label: "Crop Enrollment",
      backgroundColor: ["#887bc9", "#529ee6", "#68c1f4"],
      data: [80, 40, 60]
    }
  ]
};

new Chart(document.getElementById("crops"), {
  type: "pie",
  data: cropsData,
  options: pieChartOptions
});
